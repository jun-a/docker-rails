# railsのアプリケーションを始める導入手順

## dockerの導入手順

1. このレポジトリをcloneする
2. cloneしたレポジトリを実施したいアプリ名に変更
3. `Dockerfile` と `docker-compose.yml` ファイルの中の `myapp`部分を自分のアプリ名に変更
4. ファイル内の文字をプロダクト名に変更
` grep -l 'myapp' docker-compose.yml | xargs sed -i.bak -e 's/myapp/プロダクト名/g' `
5. postgresをDBに指定してdockerコマンドを実行
`docker-compose run web rails new . --force --no-deps --database=postgresql`
6. `docker-compose build`でビルドする
7. `config/database.yml`に以下を追加

```
  host: db
  username: postgres
  password:
```
8. `docker-compose up`でアプリを起動
9. DBを作成 `docker-compose run web rake db:create`


参考: Quickstart: Compose and Rails | Docker Documentation 
https://docs.docker.com/compose/rails/
